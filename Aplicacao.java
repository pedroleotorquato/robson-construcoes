import java.util.ArrayList;
import java.util.List;
/**
 * Aplicacao com as seguintes funcionalidades:
 * .Cadastrar cargo.
 * .Cadastrar funcionario.
 * .Mostrar um relatorio contendo o codigo do funcionario, 
 *  o nome e o valor do salario de cada funcionario.
 * .Mostrar o valor total pago de salario aos funcionarios que 
 *  pertencam a um cargo informado pelo usuario.
 * @author Pedro Leonardo 2010460
 */
public class Aplicacao {
	
	private int qtdDeCamposFunc = 3; //qtd de campos no vetor funcionario
	private List<Double> cargos = new ArrayList<Double>();
	private List<String[]> funcionarios = new ArrayList<String[]>();
		
	
	/**
	 * Adiciona um novo cargo ao vetor de cargos.
	 * @param codigoCargoIn Codigo do cargo
	 * @param salarioIn Salario referente ao cargo
	 * @return nothing
	 */
	public void addCargo(int codigoCargoIn, double salarioIn)
	{
		this.cargos.add(codigoCargoIn, Double.valueOf(salarioIn));
	}
	
	/**
	 * Adiciona um novo funcionario ao vetor de funcionarios.
	 * @param codigoFuncIn Codigo do funcionario. Nao e permitido cadastrar
	 * funcionarios com codigos repetidos.
	 * @param nomeIn Nome do funcionario
	 * @param codigoCargoIn Codigo do cargo do funcionario. O codigo do cargo
	 * deve existir no vetor de cargos para que o funcionario seja cadastrado.
	 * @return nothing
	 */
	public void addFuncionario(int codigoFuncIn, String nomeIn, int codigoCargoIn)
	{
		if(!isCodePresente(codigoFuncIn))
		{
			if(codigoCargoIn < (cargos.size()) && codigoCargoIn >=0 && cargos.size() > 0)
			{
				String[] vetor = new String[qtdDeCamposFunc];
				vetor[0] = String.valueOf(codigoFuncIn);
				vetor[1] = nomeIn;
				vetor[2] = String.valueOf(codigoCargoIn);
				this.funcionarios.add(vetor);
			}else
			{
				System.out.println("Cargo inexistente ou nao existem cargos "
						+ "cadastrados! Informe um codigo de cargo"
						+ " valido.");
			}

		}else
		{
			System.out.println("Nao foi possivel adicionar o funcionario"
					+ " pois o codigo do funcionario e repetido.");
		}

	}
	
	/**
	 * Lista as informacoes do vetor de funcionarios.
	 * @return nothing
	 */
	public void relatorio()
	{
		if(funcionarios.size() > 0)
		{
			for(int i=0; i<funcionarios.size(); i++)
			{
				String vetor[] = funcionarios.get(i);
				System.out.print("Codigo: " + vetor[0] + " | Nome: "
						+ vetor[1] + " | Codigo do Cargo: " + vetor[2] + "\n");
			}
		}else
		{
			System.out.println("Nao existem funcionarios cadastrados.");
		}
		
	}
	
	/**
	 * Imprime o somatorio de todos os funcionarios desse cargo {@code codigoIn}.
	 * @param codigoIn Codigo do cargo.
	 * @return nothing
	 */
	public void totalSalarioCargo(int codigoIn)
	{		
		if(codigoIn >= 0 && codigoIn < cargos.size())
		{
			double somatorio = 0;
			String codigo = String.valueOf(codigoIn);			
			
			for(int i=0; i<funcionarios.size(); i++)
			{
				String[] vetor = funcionarios.get(i);
				
				if(vetor[2].equalsIgnoreCase(codigo))
				{
					somatorio+= cargos.get(codigoIn);
				}
			}
						
			System.out.printf("Soma do salrio de todos os funcionrios "
					+ "do cargo %d: R$ %.2f \n",codigoIn, somatorio);
		}else
		{
			System.out.println("Codigo de cargo invalido");
		}
		
	}
	
	/**
	 * Informa se o codigo de um funario ja existe no vetor funcionario.
	 * Metodo privado.
	 * @param codigoFuncIn
	 * @return {@codetrue} se o codigo do funcionario ja existe no
	 * vetor funcionarios e {@codefalse} se o codigo do funcionario 
	 * nao existe no vetor funcionarios.
	 */
	private boolean isCodePresente(int codigoFuncIn)
	{
		String cod = String.valueOf(codigoFuncIn);
		
		for(int i=0; i<funcionarios.size(); i++)
		{
			String[] vetor = funcionarios.get(i);
			if(vetor[0].equalsIgnoreCase(cod))
			{
				return true;
			}			
		}
		
		return false;
	}
}
