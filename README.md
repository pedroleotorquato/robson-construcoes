# Robson Construções

#### Utilizando a Aplicação
Para utilizar a aplicação basta instanciar um objeto da classe `Aplicacao` :)
**O arquivo resposta para o desafio é o `Aplicacao.java`.**


#### Versão Java

A solução foi desenvolvida na IDE Eclipse, utilizando a Open JDK, versão 14.0.2 (disponível em: https://jdk.java.net/14/).
A máquina roda o Java na versão 13.0.1.
